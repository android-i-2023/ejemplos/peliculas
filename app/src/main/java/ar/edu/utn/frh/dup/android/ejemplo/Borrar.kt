package ar.edu.utn.frh.dup.android.ejemplo

import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private val retrofit = Retrofit.Builder()
    .baseUrl("https://www.omdbapi.com/")
    .addConverterFactory(MoshiConverterFactory.create())
    .build()

private val api = retrofit.create(OmdbApi::class.java)

fun buscar(
    callbackResultados: (Array<Contenido>) -> Unit,
    callbackError: (Throwable) -> Unit,
) {
    val call: Call<Resultado> = api.obtenerContenidos(
        busqueda = "Aladdin",
        pagina = 1,
        tipo = "movie"
    )

    call.enqueue(object: Callback<Resultado> {
        override fun onResponse(call: Call<Resultado>, response: Response<Resultado>) {
            if (response.isSuccessful) {
                val body = response.body()
                if (body?.Search != null) {
                    callbackResultados(body.Search)
                } else if (body?.Error != null) {
                    onFailure(call, Exception(body.Error))
                }
            } else {
                onFailure(call, HttpException(response))
            }
        }

        override fun onFailure(call: Call<Resultado>, t: Throwable) {
            callbackError(t)
        }
    })
}