package ar.edu.utn.frh.dup.android.ejemplo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class DetailActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_TITULO = "titulo"
        const val EXTRA_FECHA = "fecha"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val tituloTv = findViewById<TextView>(R.id.titulo)
        val fechaTv = findViewById<TextView>(R.id.fecha)

        val titulo = intent.getStringExtra(EXTRA_TITULO)
        val fecha = intent.getStringExtra(EXTRA_FECHA)

        if (titulo == null || fecha == null) {
            Toast.makeText(
                this,
                getString(R.string.error_faltan_datos),
                Toast.LENGTH_LONG
            ).show()
            finish()
            return
        }

        tituloTv.text = titulo
        fechaTv.text = fecha
    }
}