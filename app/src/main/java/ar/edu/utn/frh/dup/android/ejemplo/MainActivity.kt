package ar.edu.utn.frh.dup.android.ejemplo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    /**
     * Esto corre en el hilo principal o hilo de UI
     * ie. Main Thread o UI Thread
     *
     * Thread
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val busqueda = findViewById<EditText>(R.id.busqueda)
        busqueda.setOnEditorActionListener(object: TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                return true
            }
        })

        val lista: RecyclerView = findViewById(R.id.lista)
        val adapter = ContenidoAdapter(this, { contenido ->
            contenidoClickeado(contenido)
        })

        lista.adapter = adapter

        buscar(
            callbackResultados = { contenidos ->
                adapter.asignarDatos(contenidos)
            },
            callbackError = { t ->
                val mensaje = t.message

                Log.e("CURSO", "Se rompio algo", t)
                Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()
            }
        )
    }

    private fun contenidoClickeado(contenido: Contenido) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(DetailActivity.EXTRA_TITULO, contenido.titulo)
        intent.putExtra(DetailActivity.EXTRA_FECHA, contenido.fecha)
        startActivity(intent)
    }

}