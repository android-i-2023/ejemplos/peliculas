package ar.edu.utn.frh.dup.android.ejemplo

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OmdbApi {
    @GET("/")
    fun obtenerContenidos(
        @Query("apiKey")
        claveOmdb: String = "70b96a2a",

        @Query("s")
        busqueda: String,

        @Query("page")
        pagina: Int,

        @Query("type")
        tipo: String
    ): Call<Resultado>


}