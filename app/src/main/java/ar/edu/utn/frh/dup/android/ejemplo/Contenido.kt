package ar.edu.utn.frh.dup.android.ejemplo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Contenido(
    @Json(name = "Title")
    val titulo: String,

    @Json(name = "Year")
    val fecha: String,

    @Json(name = "Poster")
    val fotoUrl: String
)

@JsonClass(generateAdapter = true)
class Resultado(
    val Search: Array<Contenido>?,
    val Response: String?,
    val Error: String?,
)
