package ar.edu.utn.frh.dup.android.ejemplo

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class ContenidoAdapter(
    private val activity: Activity,
    private val contenidoClickeado: (Contenido) -> Unit
): RecyclerView.Adapter<ContenidoVH>() {

    private var datos: Array<Contenido> = emptyArray()

    fun asignarDatos(nuevosDatos: Array<Contenido>) {
        datos = nuevosDatos
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContenidoVH {
        val vista = activity.layoutInflater.inflate(R.layout.item_contenido, parent, false)
        return ContenidoVH(vista)
    }

    override fun onBindViewHolder(holder: ContenidoVH, position: Int) {
        val dato = datos[position]
        holder.tituloTv.text = dato.titulo
        holder.fechaTv.text =  dato.fecha

        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                contenidoClickeado(dato)
            }
        })

        Glide.with(holder.fotoIv)
            .load(dato.fotoUrl)
            .into(holder.fotoIv)
    }

    override fun getItemCount(): Int {
        return datos.size
    }
}
