package ar.edu.utn.frh.dup.android.ejemplo

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ContenidoVH(itemView: View): RecyclerView.ViewHolder(itemView) {
    val tituloTv: TextView = itemView.findViewById(R.id.titulo)
    val fechaTv = itemView.findViewById<TextView>(R.id.fecha)
    val fotoIv = itemView.findViewById<ImageView>(R.id.foto)
}
